var mongoose = require('mongoose')
var Schema = mongoose.Schema

const MachineSchema = new Schema({
  npm: {
    type: String,
    required: true
  },
  ts : {
    type: Date,
    required: true
  }
}, { collection: 'Machine' })

const Machine = mongoose.model('Machine', MachineSchema)

module.exports = Machine