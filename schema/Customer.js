var mongoose = require('mongoose')
var Schema = mongoose.Schema

const CustomerSchema = new Schema({
  user_id: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  balance: {
    type: Number,
    required: true
  }
}, { collection: 'Customer' })

const Customer = mongoose.model('Customer', CustomerSchema)

module.exports = Customer