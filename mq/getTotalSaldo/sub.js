var amqp = require('amqplib')
var config = require('../../config')
var mongoose = require('mongoose')
var connectDB = require('../../helpers/connectDB')
var Customer = require('../../schema/Customer')

// connect to db
connectDB()

amqp.connect(config.connect)
.then((conn) => {
  return conn.createChannel()
  .then((ch) => {
    ch.assertExchange('EX_GET_TOTAL_SALDO', 'direct')

    ch.assertQueue('', {exclusive: true})
    .then((q) => {
      ch.bindQueue(q.queue, 'EX_GET_TOTAL_SALDO', 'REQ_1306464114')      

      ch.consume(q.queue, (msg) => {
        var contentArrBuffer = Buffer.from(msg.content)
        var message = JSON.parse(contentArrBuffer)

        var user_id = message['user_id']
        var name = message['nama']
        var sender_id = message['sender_id']

        var newCustomerSchema = {
          user_id: user_id,
          name: name,
          balance: (user_id === config.thisNode.user_id) ? 1000000000 : 0
        }  
      
        Customer.findOne({ user_id: user_id }).exec()
        .then((customer) => {
          if (!customer) {
            var newCustomer = new Customer(newCustomerSchema)
            
            newCustomer.save(function(err) {
              if (err) {
                ch.publish('EX_GET_TOTAL_SALDO', `RESP_${sender_id}`, new Buffer(JSON.stringify({
                  action: 'register',
                  type: 'response',
                  ts: new Date().toLocaleString(),
                  status_register: -99,
                  message: `Failed to create Customer. Error: ${err}`
                })))              
              } else {
                ch.publish('EX_GET_TOTAL_SALDO', `RESP_${sender_id}`, new Buffer(JSON.stringify({
                  action: 'register',
                  type: 'response',
                  ts: new Date().toLocaleString(),
                  status_register: 1,
                  message: 'Register success'
                })))
              }
            })
          } else {
            ch.publish('EX_GET_TOTAL_SALDO', `RESP_${sender_id}`, new Buffer(JSON.stringify({
              action: 'register',
              type: 'response',
              ts: new Date().toLocaleString(),
              status_register: -4,
              message: 'Customer exist'
            })))
          }
        })
        .catch((err) => {
          ch.publish('EX_GET_TOTAL_SALDO', `RESP_${sender_id}`, new Buffer(JSON.stringify({
            action: 'register',
            type: 'response',
            ts: new Date().toLocaleString(),
            status_register: -4,
            message: `Database error. Might be wrong payload... or something else. Error: ${err}`
          })))            
        })
      }, {noAck: true})      
    })
  })
})
