var amqp = require('amqplib')
var config = require('../../config')
var mongoose = require('mongoose')
var connectDB = require('../../helpers/connectDB')
var Customer = require('../../schema/Customer')

// connect to db
connectDB()

amqp.connect(config.connect)
.then((conn) => {
  return conn.createChannel()
  .then((ch) => {
    ch.assertExchange('EX_GET_SALDO', 'direct')

    ch.assertQueue('', {exclusive: true})
    .then((q) => {
      ch.bindQueue(q.queue, 'EX_GET_SALDO', 'REQ_1306464114')      

      ch.consume(q.queue, (msg) => {
        var contentArrBuffer = Buffer.from(msg.content)
        var message = JSON.parse(contentArrBuffer)

        var user_id = message['user_id']        
        var sender_id = message['sender_id'] 
                
        Customer.findOne({ user_id: user_id }).exec()
        .then((customer) => {
          if (!customer) {            
            ch.publish('EX_GET_SALDO', `RESP_${sender_id}`, new Buffer(JSON.stringify({
              action: 'transfer',
              type: 'response',
              ts: new Date().toLocaleString(),
              nilai_saldo: -1,
              message: 'User not registered'
            })))                          
          } else {                                      
            ch.publish('EX_GET_SALDO', `RESP_${sender_id}`, new Buffer(JSON.stringify({
              action: 'transfer',
              type: 'response',
              ts: new Date().toLocaleString(),
              nilai_saldo: customer.balance,
              message: 'Success get saldo'
            })))                                  
          }
        })
        .catch((err) => {
          ch.publish('EX_GET_SALDO', `RESP_${sender_id}`, new Buffer(JSON.stringify({
            action: 'transfer',
            type: 'response',
            ts: new Date().toLocaleString(),
            nilai_saldo: -4,
            message: `Database error. Might be wrong payload... or something else. Error: ${err}`
          })))            
        })        
      
      }, {noAck: true})      
    })
  })
})
