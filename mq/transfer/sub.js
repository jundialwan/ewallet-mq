var amqp = require('amqplib')
var config = require('../../config')
var mongoose = require('mongoose')
var connectDB = require('../../helpers/connectDB')
var Customer = require('../../schema/Customer')

// connect to db
connectDB()

amqp.connect(config.connect)
.then((conn) => {
  return conn.createChannel()
  .then((ch) => {
    ch.assertExchange('EX_TRANSFER', 'direct')

    ch.assertQueue('', {exclusive: true})
    .then((q) => {
      ch.bindQueue(q.queue, 'EX_TRANSFER', 'REQ_1306464114')      

      ch.consume(q.queue, (msg) => {
        var contentArrBuffer = Buffer.from(msg.content)
        var message = JSON.parse(contentArrBuffer)

        var user_id = message['user_id']
        var amount = message['nilai']
        var sender_id = message['sender_id'] 
                
        Customer.findOne({ user_id: user_id }).exec()
        .then((customer) => {
          if (!customer) {            
            ch.publish('EX_TRANSFER', `RESP_${sender_id}`, new Buffer(JSON.stringify({
              action: 'transfer',
              type: 'response',
              ts: new Date().toLocaleString(),
              status_transfer: -1,
              message: 'User not registered'
            })))                          
          } else {                          
            customer.balance = parseInt(customer.balance) + parseInt(amount)

            customer.save(function(err) {
              if (err) {
                ch.publish('EX_TRANSFER', `RESP_${sender_id}`, new Buffer(JSON.stringify({
                  action: 'transfer',
                  type: 'response',
                  ts: new Date().toLocaleString(),
                  status_transfer: -99,
                  message: 'Error when update Customer'
                }))) 
              } else {
                ch.publish('EX_TRANSFER', `RESP_${sender_id}`, new Buffer(JSON.stringify({
                  action: 'transfer',
                  type: 'response',
                  ts: new Date().toLocaleString(),
                  status_transfer: 1,
                  message: 'Transfer success'
                })))            
              }
            })            
          }
        })
        .catch((err) => {
          ch.publish('EX_TRANSFER', `RESP_${sender_id}`, new Buffer(JSON.stringify({
            action: 'transfer',
            type: 'response',
            ts: new Date().toLocaleString(),
            status_tranfer: -4,
            message: `Database error. Might be wrong payload... or something else. Error: ${err}`
          })))            
        })        
      
      }, {noAck: true})      
    })
  })
})
