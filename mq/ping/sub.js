var amqp = require('amqplib')
var config = require('../../config')
var Machine = require('../../schema/Machine')
var mongoose = require('mongoose')
var connectDB = require('../../helpers/connectDB')

// connect to db
connectDB()

amqp.connect(config.connect)
.then((conn) => {
  return conn.createChannel()
  .then((ch) => {
    ch.assertExchange('EX_PING', 'fanout', {durable: false})

    return ch.assertQueue('', {exclusive: true})
    .then((q) => {
      ch.bindQueue(q.queue, 'EX_PING', '')

      ch.consume(q.queue, (msg) => {        
        const contentArrBuffer = Buffer.from(msg.content)
        const message = JSON.parse(contentArrBuffer)

        // write to database
        Machine.findOne({ npm: message.npm }).exec()
          .then((machine) => {
            if (machine) {
              machine.ts = message.ts

              machine.save(err => console.log(err))
            } else {
              let newMachine = new Machine({
                npm: message.npm,
                ts: message.ts
              })

              newMachine.save(err => console.log(err))
            }
          })
          .catch(err => console.log(err))
      }, {noAck: true})
    })    
  })  
})
.catch(e => console.log(e))