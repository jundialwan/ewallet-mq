#! /usr/bin/env node

var amqp = require('amqplib')
var program = require('commander')
var getQuorum = require('../helpers/getQuorum')
var config = require('../config')

var Customer = require('../schema/Customer')
var Machine = require('../schema/Machine')

program.version('0.1.0')  
  .option('--dest <userId>', 'User ID destination')

var checkQuorum = function(cmd) {
  return getQuorum()
  .then((quorum) => {
    console.log('Quorum: ', quorum)

    switch(cmd) {
      case 'transfer':
      case 'balance':
      case 'register':
        return (quorum <= 0.5) ? false : true
        break
      case 'totalbalance':
        return (quorum < 1) ? false : true
        break
    }
  })
  .then((permitted) => {
    if (!permitted) {
      console.log('Quorum not satisfied for ', cmd)
      process.exit(1)  
    } else {
      return true
    }
  })
  .catch((err) => {
    console.log('Check quorum error')
    process.exit(1)
  })
}

program.command('register <userid> <name>').action(function(userid, name) {
  if (program.dest) {
    checkQuorum('register')
    .then((result) => {
      if (result) {
        amqp.connect(config.connect)
        .then((conn) => {
          // consumer
          conn.createChannel()
          .then((ch) => {
            ch.assertExchange('EX_REGISTER', 'direct', {durable: true})
            
            ch.assertQueue('', {exclusive: true})
            .then((q) => {
              // response consumer
              ch.bindQueue(q.queue, 'EX_REGISTER', 'RESP_1306464114')

              ch.consume(q.queue, (msg) => {
                var contentArrBuffer = Buffer.from(msg.content)
                var message = JSON.parse(contentArrBuffer)
                
                console.log(message)
                process.exit(1)
              }, {noAck: true})            
            })                         
          })

          // publisher
          conn.createChannel()
          .then((ch) => {
            ch.assertExchange('EX_REGISTER', 'direct', {durable: true})
            
            console.log('Sending request...')
  
            console.log('Destination: ', program.dest)
            
            // publisher      
            ch.publish('EX_REGISTER', `REQ_${program.dest}`, new Buffer(JSON.stringify({
              action: 'register',
              user_id: userid,
              nama: name,
              sender_id: '1306464114',
              type: 'request',
              ts: new Date().toLocaleString()
            })))
  
            console.log('Request [register] sent. Waiting for response...')                            
          })          
        })
        .catch(err => console.log(err))
      }
    })
    .catch((err) => {
      console.log('Check quorum error')
      process.exit(1)
    })
  } else {
    console.log('No destination given')
    process.exit(1)
  }
})

program.command('transfer <userid> <amount>').action(function(userid, amount) {
  if (program.dest) {
    checkQuorum('transfer')
    .then((result) => {
      if (result) {
        amqp.connect(config.connect)
        .then((conn) => {
          // consumer
          conn.createChannel()
          .then((ch) => {
            ch.assertExchange('EX_TRANSFER', 'direct', {durable: true})
            
            ch.assertQueue('', {exclusive: true})
            .then((q) => {
              // response consumer
              ch.bindQueue(q.queue, 'EX_TRANSFER', 'RESP_1306464114')

              ch.consume(q.queue, (msg) => {
                var contentArrBuffer = Buffer.from(msg.content)
                var message = JSON.parse(contentArrBuffer)
                
                if (message.status_transfer === 1) {
                  Customer.findOne({ user_id: userid }).exec()
                  .then((customer) => {
                    customer.balance = parseInt(customer.balance) - parseInt(amount)

                    customer.save(function(err) {
                      if (err) {
                        console.log('Error when updating balance in this machine')
                        console.log(message)

                        process.exit(1)
                      } else {
                        console.log(message)

                        process.exit(1)
                      }
                    })
                  })
                  .catch(err => console.log(err))
                } else {
                  console.log(message)
                  process.exit(1)
                }

              }, {noAck: true})            
            })                         
          })

          // publisher
          conn.createChannel()
          .then((ch) => {
            ch.assertExchange('EX_TRANSFER', 'direct', {durable: true})

            Customer.findOne({ user_id: userid }).exec()
            .then((customer) => {
              if (parseInt(amount) < 0 || parseInt(amount) > 1000000 || parseInt(amount) > parseInt(customer.balance)) {
                console.log('Transfer amount exceed 1000000000 or below 0. status_register: -5') 
              } else {
                console.log('Sending request...')
      
                console.log('Destination: ', program.dest)
                
                // publisher      
                ch.publish('EX_TRANSFER', `REQ_${program.dest}`, new Buffer(JSON.stringify({
                  action: 'transfer',
                  user_id: userid,
                  nilai: amount,
                  sender_id: '1306464114',
                  type: 'request',
                  ts: new Date().toLocaleString()
                })))
      
                console.log('Request [transfer] sent. Waiting for response...')
              } 
            })
            .catch(err => console.log(err))                       
          })          
        })
        .catch(err => console.log(err))
      }
    })
    .catch((err) => {
      console.log('Check quorum error')
      process.exit(1)
    })
  } else {
    console.log('No destination given')
    process.exit(1)
  }  
})

program.command('balance <userid>').action(function(userid) {
  if (program.dest) {
    checkQuorum('balance')
    .then((result) => {
      if (result) {
        amqp.connect(config.connect)
        .then((conn) => {
          // consumer
          conn.createChannel()
          .then((ch) => {
            ch.assertExchange('EX_GET_SALDO', 'direct', {durable: true})
            
            ch.assertQueue('', {exclusive: true})
            .then((q) => {
              // response consumer
              ch.bindQueue(q.queue, 'EX_GET_SALDO', 'RESP_1306464114')

              ch.consume(q.queue, (msg) => {
                var contentArrBuffer = Buffer.from(msg.content)
                var message = JSON.parse(contentArrBuffer)
                
                console.log(message)
                process.exit(1)
              }, {noAck: true})            
            })                         
          })

          // publisher
          conn.createChannel()
          .then((ch) => {
            ch.assertExchange('EX_GET_SALDO', 'direct', {durable: true})
            
            console.log('Sending request...')
  
            console.log('Destination: ', program.dest)
            
            // publisher      
            ch.publish('EX_GET_SALDO', `REQ_${program.dest}`, new Buffer(JSON.stringify({
              action: 'get_saldo',
              user_id: userid,              
              sender_id: '1306464114',
              type: 'request',
              ts: new Date().toLocaleString()
            })))
  
            console.log('Request [GET SALDO] sent. Waiting for response...')                            
          })          
        })
        .catch(err => console.log(err))
      }
    })
    .catch((err) => {
      console.log('Check quorum error')
      process.exit(1)
    })
  } else {
    console.log('No destination given')
    process.exit(1)
  }
})

program.command('totalbalance <userid>').action(function(userid) { 
  let machines = {}  
  let totalSaldo = 0
  let totalMachineDone = 0

  checkQuorum('totalbalance')
  .then((result) => {
    if (result) {

      // populate machine for get total saldo
      Machine.find({}).exec()
      .then((machines) => {
        machines.forEach((m) => {machines[m.user_id] = 0})
      })
      .then(() => {
        amqp.connect(config.connect)
        .then((conn) => {
          // consumer
          conn.createChannel()
          .then((ch) => {
            ch.assertExchange('EX_GET_SALDO', 'direct', {durable: true})
            
            ch.assertQueue('', {exclusive: true})
            .then((q) => {
              // response consumer
              ch.bindQueue(q.queue, 'EX_GET_SALDO', 'RESP_1306464114')
  
              ch.consume(q.queue, (msg) => {
                var contentArrBuffer = Buffer.from(msg.content)
                var message = JSON.parse(contentArrBuffer)
                
                if (parseInt(message.nilai_saldo) < 0) {
                  console.log('Failed to Get Total Saldo. One or more machine is not proper.')
                } else {
                  totalSaldo += parseInt(message.nilai_saldo)
                }                

                ++totalMachineDone

                console.log(`Get saldo: ${totalMachineDone}/${Object.keys(machines).length}`)
                console.log(`Saldo: ${totalSaldo}`)
              }, {noAck: true})            
            })                         
          })
  
          // publisher
          conn.createChannel()
          .then((ch) => {
            ch.assertExchange('EX_GET_SALDO', 'direct', {durable: true})
            
            console.log('Sending request to all machine...')
            
            Object.keys(machines).forEach((m) => {
              // publisher      
              ch.publish('EX_GET_SALDO', `REQ_${m.user_id}`, new Buffer(JSON.stringify({
                action: 'register',
                user_id: userid,                
                sender_id: '1306464114',
                type: 'request',
                ts: new Date().toLocaleString()
              })))
            })
            
            console.log('Request [register] sent. Waiting for response...')                            
          })          
        })
        .catch(err => console.log(err))
      })
      .catch(err => console.log(err))


    }
  })
  .catch((err) => {
    console.log('Check quorum error')
    process.exit(1)
  })
})

  
program.parse(process.argv)