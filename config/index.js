module.exports = {
  connect: {
    protocol: 'amqp',
    hostname: '172.17.0.3',
    port: 5672,
    username: 'sisdis',
    password: 'sisdis',
    locale: 'en_US'
  },
  thisNode: {
    user_id: '1306464114',
    name: 'Jundi Alwan',
    ip: '172.17.0.62'
  }  
}