var mongoose = require('mongoose')
var moment = require('moment')
var Machine = require('../schema/Machine')
var connectDB = require('../helpers/connectDB')

// connect to db
connectDB()

var getQuorum = function() {
  return Machine.find({}).exec()
    .then((machines) => {
      let active = 0
  
      machines.map((machine) => {
        var now = moment()
        var lastPing = moment(machine.ts)

        if (now.diff(lastPing) <= 10000) {
          ++active
        }
      })

      return active / (machines.length)
    })
    .catch(err => 'error')
}

module.exports = getQuorum