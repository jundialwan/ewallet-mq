var mongoose = require('mongoose')

// connect to db
module.exports = function() {
  mongoose.connect('mongodb://localhost:27017/ewallet', { useMongoClient: true })
  mongoose.Promise = global.Promise
}